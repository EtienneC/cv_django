#from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    #return HttpResponse("<h1>Le Blog</h1>")
    return render(request, "blog/index.html")

def cv(request, numero_cv):
    if numero_cv in ["01", "02"]:            
        return render(request, f"blog/cv_{numero_cv}.html")
    return render(request, "blog/cv_not_found.html")
