from django.urls import path
from .views import index, cv

urlpatterns = [
    path('', index, name="blog-index"),
    path('cv_<str:numero_cv>/', cv, name="cv_01")
]