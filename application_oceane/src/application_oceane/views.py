#from django.http import HttpResponse
from datetime import datetime

from django.shortcuts import render


def index(request):
    #return HttpResponse("<h1>Titre de mon site pour le projet Océane</h1>")
    return render(request, "application_CV/index.html", context={"prenom":"ma coquine", "date":datetime.today()})
